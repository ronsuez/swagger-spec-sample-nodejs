'use strict';

exports.oauthAccessTokenPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * clientId (String)
  * clientSecret (String)
  * code (String)
  * accept (String)
  * grantType (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "access_token" : "aeiou",
  "refresh_token" : "aeiou",
  "scope" : [ "aeiou" ],
  "token_type" : "aeiou",
  "expires_in" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.oauthAuthorizeGET = function(args, res, next) {
  /**
   * parameters expected in the args:
  * clientId (String)
  * redirectUri (String)
  * scope (String)
  * state (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "code" : "aeiou",
  "state" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.oauthRefreshAccessTokenPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * grantType (String)
  * refreshToken (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "refresh_token" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

