'use strict';

exports.authLoginPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * username (String)
  * password (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.authRegisterPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * username (String)
  * userEmail (String)
  * password (String)
  * confirmPassword (String)
  **/
  // no response value expected for this operation
  res.end();
}

