'use strict';

exports.holdersHolderIdApplicationsGET = function(args, res, next) {
  /**
   * parameters expected in the args:
  * holderId (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "offset" : 123,
  "size" : 123,
  "limit" : 123,
  "items" : [ {
    "apiKeys" : [ "" ],
    "description" : "aeiou",
    "created_at" : "2000-01-23T04:56:07.000+0000",
    "groups" : [ {
      "href" : "aeiou"
    } ],
    "holder" : [ {
      "href" : "aeiou"
    } ],
    "authTokens" : [ "" ],
    "loginAttemps" : [ {
      "href" : "aeiou"
    } ],
    "name" : "aeiou",
    "passwordResetTokens" : [ "" ],
    "id" : "aeiou",
    "accounts" : [ {
      "href" : "aeiou"
    } ],
    "modified_at" : "2000-01-23T04:56:07.000+0000",
    "status" : "aeiou"
  } ]
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.holdersHolderIdApplicationsPOST = function(args, res, next) {
  /**
   * parameters expected in the args:
  * holderId (String)
  * appName (String)
  * appDescription (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "apiKeys" : [ "" ],
  "description" : "aeiou",
  "created_at" : "2000-01-23T04:56:07.000+0000",
  "groups" : [ {
    "href" : "aeiou"
  } ],
  "holder" : [ {
    "href" : "aeiou"
  } ],
  "authTokens" : [ "" ],
  "loginAttemps" : [ {
    "href" : "aeiou"
  } ],
  "name" : "aeiou",
  "passwordResetTokens" : [ "" ],
  "id" : "aeiou",
  "accounts" : [ {
    "href" : "aeiou"
  } ],
  "modified_at" : "2000-01-23T04:56:07.000+0000",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.holdersHolderIdGET = function(args, res, next) {
  /**
   * parameters expected in the args:
  * holderId (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "name" : "aeiou",
  "created_at" : "2000-01-23T04:56:07.000+0000",
  "groups" : [ {
    "href" : "aeiou"
  } ],
  "id" : "aeiou",
  "accounts" : [ {
    "href" : "aeiou"
  } ],
  "modified_at" : "2000-01-23T04:56:07.000+0000",
  "key" : "aeiou",
  "applications" : [ {
    "href" : "aeiou"
  } ]
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

