'use strict';

var url = require('url');


var UserEndpoints = require('./UserEndpointsService');


module.exports.authLoginPOST = function authLoginPOST (req, res, next) {
  UserEndpoints.authLoginPOST(req.swagger.params, res, next);
};

module.exports.authRegisterPOST = function authRegisterPOST (req, res, next) {
  UserEndpoints.authRegisterPOST(req.swagger.params, res, next);
};
