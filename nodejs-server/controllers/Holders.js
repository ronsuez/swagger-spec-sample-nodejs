'use strict';

var url = require('url');


var Holders = require('./HoldersService');


module.exports.holdersHolderIdApplicationsGET = function holdersHolderIdApplicationsGET (req, res, next) {
  Holders.holdersHolderIdApplicationsGET(req.swagger.params, res, next);
};

module.exports.holdersHolderIdApplicationsPOST = function holdersHolderIdApplicationsPOST (req, res, next) {
  Holders.holdersHolderIdApplicationsPOST(req.swagger.params, res, next);
};

module.exports.holdersHolderIdGET = function holdersHolderIdGET (req, res, next) {
  Holders.holdersHolderIdGET(req.swagger.params, res, next);
};
