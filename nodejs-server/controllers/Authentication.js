'use strict';

var url = require('url');


var Authentication = require('./AuthenticationService');


module.exports.oauthAccessTokenPOST = function oauthAccessTokenPOST (req, res, next) {
  Authentication.oauthAccessTokenPOST(req.swagger.params, res, next);
};

module.exports.oauthAuthorizeGET = function oauthAuthorizeGET (req, res, next) {
  Authentication.oauthAuthorizeGET(req.swagger.params, res, next);
};

module.exports.oauthRefreshAccessTokenPOST = function oauthRefreshAccessTokenPOST (req, res, next) {
  Authentication.oauthRefreshAccessTokenPOST(req.swagger.params, res, next);
};
