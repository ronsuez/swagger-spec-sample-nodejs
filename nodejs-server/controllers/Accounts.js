'use strict';

var url = require('url');


var Accounts = require('./AccountsService');


module.exports.applicationsApplicationIdAccountsLoginAttempsPOST = function applicationsApplicationIdAccountsLoginAttempsPOST (req, res, next) {
  Accounts.applicationsApplicationIdAccountsLoginAttempsPOST(req.swagger.params, res, next);
};

module.exports.applicationsApplicationIdAccountsPOST = function applicationsApplicationIdAccountsPOST (req, res, next) {
  Accounts.applicationsApplicationIdAccountsPOST(req.swagger.params, res, next);
};
