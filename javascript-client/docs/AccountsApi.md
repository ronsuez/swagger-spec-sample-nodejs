# AuthIdentityService.AccountsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**applicationsApplicationIdAccountsLoginAttempsPost**](AccountsApi.md#applicationsApplicationIdAccountsLoginAttempsPost) | **POST** /applications/{applicationId}/accounts/loginAttemps | Attemps an account login
[**applicationsApplicationIdAccountsPost**](AccountsApi.md#applicationsApplicationIdAccountsPost) | **POST** /applications/{applicationId}/accounts | Create a User Account into an application


<a name="applicationsApplicationIdAccountsLoginAttempsPost"></a>
# **applicationsApplicationIdAccountsLoginAttempsPost**
> Account applicationsApplicationIdAccountsLoginAttempsPost(applicationId)

Attemps an account login

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');
var defaultClient = AuthIdentityService.ApiClient.default;

// Configure API key authorization: api_key
var api_key = defaultClient.authentications['api_key'];
api_key.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key.apiKeyPrefix['X-Api-Key'] = "Token"

// Configure API key authorization: api_key_secret
var api_key_secret = defaultClient.authentications['api_key_secret'];
api_key_secret.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key_secret.apiKeyPrefix['X-Api-Secret'] = "Token"

var apiInstance = new AuthIdentityService.AccountsApi()

var applicationId = "applicationId_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.applicationsApplicationIdAccountsLoginAttempsPost(applicationId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **applicationId** | **String**|  | 

### Return type

[**Account**](Account.md)

### Authorization

[api_key](../README.md#api_key), [api_key_secret](../README.md#api_key_secret)

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="applicationsApplicationIdAccountsPost"></a>
# **applicationsApplicationIdAccountsPost**
> Account applicationsApplicationIdAccountsPost(applicationId)

Create a User Account into an application

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');
var defaultClient = AuthIdentityService.ApiClient.default;

// Configure API key authorization: api_key
var api_key = defaultClient.authentications['api_key'];
api_key.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key.apiKeyPrefix['X-Api-Key'] = "Token"

// Configure API key authorization: api_key_secret
var api_key_secret = defaultClient.authentications['api_key_secret'];
api_key_secret.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key_secret.apiKeyPrefix['X-Api-Secret'] = "Token"

var apiInstance = new AuthIdentityService.AccountsApi()

var applicationId = "applicationId_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.applicationsApplicationIdAccountsPost(applicationId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **applicationId** | **String**|  | 

### Return type

[**Account**](Account.md)

### Authorization

[api_key](../README.md#api_key), [api_key_secret](../README.md#api_key_secret)

### HTTP reuqest headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/xml

