# AuthIdentityService.AccessTokenResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 
**tokenType** | **String** |  | [optional] 
**expiresIn** | **String** |  | [optional] 
**scope** | **[String]** |  | [optional] 


