# AuthIdentityService.CreatedResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Integer** |  | [optional] 
**headers** | **[String]** |  | [optional] 
**body** | **String** |  | [optional] 


