# AuthIdentityService.Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**givenName** | **String** |  | 
**surname** | **String** |  | 
**username** | **String** |  | [optional] 
**email** | **String** |  | 
**password** | **String** |  | 
**status** | **String** |  | [optional] 


