# AuthIdentityService.RelatedGroupsRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  | [default to &#39;https://api.authcloud.com/api/v1/groups/5f2mgTvyVWBL4dfUATWLk3&#39;]


