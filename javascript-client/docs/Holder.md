# AuthIdentityService.Holder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [default to &#39;5f2mgTvyVWBL4dfUATWLk3&#39;]
**name** | **String** |  | [default to &#39;recloud-tenant&#39;]
**key** | **String** |  | [default to &#39;recloud-tenant&#39;]
**createdAt** | **Date** |  | 
**modifiedAt** | **Date** |  | 
**applications** | [**[RelatedApplicationsRef]**](RelatedApplicationsRef.md) |  | 
**accounts** | [**[RelatedAccountsRef]**](RelatedAccountsRef.md) |  | 
**groups** | [**[RelatedGroupsRef]**](RelatedGroupsRef.md) |  | 


