# AuthIdentityService.Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Integer** |  | 
**code** | **Integer** |  | 
**message** | **String** |  | 
**developerMessage** | **String** | A clear, plain text explanation with technical details that might assist a developer calling the Rerecloud API. | 


