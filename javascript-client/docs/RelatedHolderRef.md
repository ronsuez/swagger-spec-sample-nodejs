# AuthIdentityService.RelatedHolderRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  | [default to &#39;https://api.authcloud.com/api/v1/holders/5f2mgTvyVWBL4dfUATWLk3&#39;]


