# AuthIdentityService.Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [default to &#39;5fabYFAmuGfqWG6xoILvTf&#39;]
**name** | **String** |  | [default to &#39;My Application&#39;]
**description** | **String** |  | [default to &#39;Application description&#39;]
**status** | **String** |  | [default to &#39;ENABLED&#39;]
**createdAt** | **Date** |  | 
**modifiedAt** | **Date** |  | 
**accounts** | [**[RelatedAccountsRef]**](RelatedAccountsRef.md) |  | 
**groups** | [**[RelatedGroupsRef]**](RelatedGroupsRef.md) |  | 
**loginAttemps** | [**[RelatedApplicationsRef]**](RelatedApplicationsRef.md) |  | 
**apiKeys** | [**[RelatedApplicationsRef]**](RelatedApplicationsRef.md) |  | 
**authTokens** | [**[RelatedApplicationsRef]**](RelatedApplicationsRef.md) |  | 
**passwordResetTokens** | [**[RelatedApplicationsRef]**](RelatedApplicationsRef.md) |  | 
**holder** | [**[RelatedHolderRef]**](RelatedHolderRef.md) |  | 


