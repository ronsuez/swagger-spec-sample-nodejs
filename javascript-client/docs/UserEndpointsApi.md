# AuthIdentityService.UserEndpointsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authLoginPost**](UserEndpointsApi.md#authLoginPost) | **POST** /auth/login | Login endpoint for users
[**authRegisterPost**](UserEndpointsApi.md#authRegisterPost) | **POST** /auth/register | Register endpoint for users


<a name="authLoginPost"></a>
# **authLoginPost**
> authLoginPost(username, password)

Login endpoint for users



### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.UserEndpointsApi()

var username = "username_example"; // {String} 

var password = "password_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.authLoginPost(username, password, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **password** | **String**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="authRegisterPost"></a>
# **authRegisterPost**
> authRegisterPost(username, userEmail, password, confirmPassword)

Register endpoint for users



### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.UserEndpointsApi()

var username = "username_example"; // {String} 

var userEmail = "userEmail_example"; // {String} 

var password = "password_example"; // {String} 

var confirmPassword = "confirmPassword_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.authRegisterPost(username, userEmail, password, confirmPassword, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**|  | 
 **userEmail** | **String**|  | 
 **password** | **String**|  | 
 **confirmPassword** | **String**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

