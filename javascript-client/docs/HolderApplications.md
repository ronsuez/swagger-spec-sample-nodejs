# AuthIdentityService.HolderApplications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **Integer** |  | [default to 0]
**limit** | **Integer** |  | [default to 0]
**size** | **Integer** |  | [default to 3]
**items** | [**[Application]**](Application.md) |  | 


