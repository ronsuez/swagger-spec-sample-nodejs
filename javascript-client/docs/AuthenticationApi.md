# AuthIdentityService.AuthenticationApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauthAccessTokenPost**](AuthenticationApi.md#oauthAccessTokenPost) | **POST** /oauth/access_token | Get Acess token to use Rerecloud API
[**oauthAuthorizeGet**](AuthenticationApi.md#oauthAuthorizeGet) | **GET** /oauth/authorize | Request user Authorization
[**oauthRefreshAccessTokenPost**](AuthenticationApi.md#oauthRefreshAccessTokenPost) | **POST** /oauth/refreshAccessToken | Refresh an access token


<a name="oauthAccessTokenPost"></a>
# **oauthAccessTokenPost**
> AccessTokenResponse oauthAccessTokenPost(opts)

Get Acess token to use Rerecloud API

Before your application can access private data using a Rerecloud API, it must obtain an access token that grants access to that API. A single access token can grant varying degrees of access to multiple APIs. A variable parameter called scope controls the set of resources and operations that an access token permits. During the access-token request, your application sends one or more values in the scope parameter.

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.AuthenticationApi()

var opts = { 
  'clientId': "clientId_example", // {String} 
  'clientSecret': "clientSecret_example", // {String} 
  'code': "code_example", // {String} Code recieved in the authorization step
  'accept': "accept_example", // {String} expected Response format
  'grantType': "grantType_example" // {String} auth grant_type [client_credentials, oauth]
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.oauthAccessTokenPost(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | [optional] 
 **clientSecret** | **String**|  | [optional] 
 **code** | **String**| Code recieved in the authorization step | [optional] 
 **accept** | **String**| expected Response format | [optional] 
 **grantType** | **String**| auth grant_type [client_credentials, oauth] | [optional] 

### Return type

[**AccessTokenResponse**](AccessTokenResponse.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json, application/xml

<a name="oauthAuthorizeGet"></a>
# **oauthAuthorizeGet**
> AuthorizeResponse oauthAuthorizeGet(clientId, redirectUri, opts)

Request user Authorization



### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.AuthenticationApi()

var clientId = "clientId_example"; // {String} 

var redirectUri = "redirectUri_example"; // {String} 

var opts = { 
  'scope': "scope_example", // {String} 
  'state': "state_example" // {String} 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.oauthAuthorizeGet(clientId, redirectUri, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 
 **redirectUri** | **String**|  | 
 **scope** | **String**|  | [optional] 
 **state** | **String**|  | [optional] 

### Return type

[**AuthorizeResponse**](AuthorizeResponse.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="oauthRefreshAccessTokenPost"></a>
# **oauthRefreshAccessTokenPost**
> RefreshTokenResponse oauthRefreshAccessTokenPost(opts)

Refresh an access token

Access tokens have limited lifetimes. If your application needs access to a Rereca API beyond the lifetime of a single access token, it can obtain a refresh token. A refresh token allows your application to obtain new access tokens.

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.AuthenticationApi()

var opts = { 
  'grantType': "grantType_example", // {String} 
  'refreshToken': "refreshToken_example" // {String} 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.oauthRefreshAccessTokenPost(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grantType** | **String**|  | [optional] 
 **refreshToken** | **String**|  | [optional] 

### Return type

[**RefreshTokenResponse**](RefreshTokenResponse.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

