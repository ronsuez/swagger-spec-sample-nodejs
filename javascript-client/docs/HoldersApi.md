# AuthIdentityService.HoldersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**holdersHolderIdApplicationsGet**](HoldersApi.md#holdersHolderIdApplicationsGet) | **GET** /holders/{holderId}/applications | Retrieve the list of holder Applications
[**holdersHolderIdApplicationsPost**](HoldersApi.md#holdersHolderIdApplicationsPost) | **POST** /holders/{holderId}/applications | Creates a new Application
[**holdersHolderIdGet**](HoldersApi.md#holdersHolderIdGet) | **GET** /holders/{holderId} | Get the Holder info and hyperlinks


<a name="holdersHolderIdApplicationsGet"></a>
# **holdersHolderIdApplicationsGet**
> HolderApplications holdersHolderIdApplicationsGet(holderId)

Retrieve the list of holder Applications

An Application is a real-world software application that communicates with Stormpath to offload user management, authentication, and security workflows. Each application that communicates with Stormpath is represented within Stormpath so you may manage its security needs

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');

var apiInstance = new AuthIdentityService.HoldersApi()

var holderId = "holderId_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.holdersHolderIdApplicationsGet(holderId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holderId** | **String**|  | 

### Return type

[**HolderApplications**](HolderApplications.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="holdersHolderIdApplicationsPost"></a>
# **holdersHolderIdApplicationsPost**
> Application holdersHolderIdApplicationsPost(holderId, opts)

Creates a new Application

Creates a new Application

### Example
```javascript
var AuthIdentityService = require('auth/identity-service');
var defaultClient = AuthIdentityService.ApiClient.default;

// Configure API key authorization: api_key
var api_key = defaultClient.authentications['api_key'];
api_key.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key.apiKeyPrefix['X-Api-Key'] = "Token"

// Configure API key authorization: api_key_secret
var api_key_secret = defaultClient.authentications['api_key_secret'];
api_key_secret.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key_secret.apiKeyPrefix['X-Api-Secret'] = "Token"

var apiInstance = new AuthIdentityService.HoldersApi()

var holderId = "holderId_example"; // {String} 

var opts = { 
  'appName': "appName_example", // {String} Desired App name
  'appDescription': "appDescription_example" // {String} Desired app description
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.holdersHolderIdApplicationsPost(holderId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holderId** | **String**|  | 
 **appName** | **String**| Desired App name | [optional] 
 **appDescription** | **String**| Desired app description | [optional] 

### Return type

[**Application**](Application.md)

### Authorization

[api_key](../README.md#api_key), [api_key_secret](../README.md#api_key_secret)

### HTTP reuqest headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/xml

<a name="holdersHolderIdGet"></a>
# **holdersHolderIdGet**
> Holder holdersHolderIdGet(holderId)

Get the Holder info and hyperlinks



### Example
```javascript
var AuthIdentityService = require('auth/identity-service');
var defaultClient = AuthIdentityService.ApiClient.default;

// Configure API key authorization: api_key
var api_key = defaultClient.authentications['api_key'];
api_key.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key.apiKeyPrefix['X-Api-Key'] = "Token"

// Configure API key authorization: api_key_secret
var api_key_secret = defaultClient.authentications['api_key_secret'];
api_key_secret.apiKey = "YOUR API KEY"
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//api_key_secret.apiKeyPrefix['X-Api-Secret'] = "Token"

// Configure OAuth2 access token for authorization: accessToken
var accessToken = defaultClient.authentications['accessToken'];
accessToken.accessToken = "YOUR ACCESS TOKEN"

var apiInstance = new AuthIdentityService.HoldersApi()

var holderId = "holderId_example"; // {String} 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.holdersHolderIdGet(holderId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **holderId** | **String**|  | 

### Return type

[**Holder**](Holder.md)

### Authorization

[api_key](../README.md#api_key), [api_key_secret](../README.md#api_key_secret), [accessToken](../README.md#accessToken)

### HTTP reuqest headers

 - **Content-Type**: application/json
 - **Accept**: application/json, application/xml

