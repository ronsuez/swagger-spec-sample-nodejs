# AuthIdentityService.AuthorizeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**state** | **String** |  | [optional] 


