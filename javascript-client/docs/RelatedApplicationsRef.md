# AuthIdentityService.RelatedApplicationsRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  | [default to &#39;https://api.authcloud.com/api/v1/applications/5f2mgTvyVWBL4dfUATWLk3&#39;]


