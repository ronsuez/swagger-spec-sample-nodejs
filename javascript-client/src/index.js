(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['./ApiClient', './model/AccessTokenResponse', './model/Account', './model/Application', './model/AuthorizeResponse', './model/CreatedResponse', './model/Error', './model/Holder', './model/HolderApplications', './model/RefreshTokenResponse', './model/RelatedAccountsRef', './model/RelatedApplicationsRef', './model/RelatedGroupsRef', './model/RelatedHolderRef', './api/AccountsApi', './api/AuthenticationApi', './api/HoldersApi', './api/UserEndpointsApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/AccessTokenResponse'), require('./model/Account'), require('./model/Application'), require('./model/AuthorizeResponse'), require('./model/CreatedResponse'), require('./model/Error'), require('./model/Holder'), require('./model/HolderApplications'), require('./model/RefreshTokenResponse'), require('./model/RelatedAccountsRef'), require('./model/RelatedApplicationsRef'), require('./model/RelatedGroupsRef'), require('./model/RelatedHolderRef'), require('./api/AccountsApi'), require('./api/AuthenticationApi'), require('./api/HoldersApi'), require('./api/UserEndpointsApi'));
  }
}(function(ApiClient, AccessTokenResponse, Account, Application, AuthorizeResponse, CreatedResponse, Error, Holder, HolderApplications, RefreshTokenResponse, RelatedAccountsRef, RelatedApplicationsRef, RelatedGroupsRef, RelatedHolderRef, AccountsApi, AuthenticationApi, HoldersApi, UserEndpointsApi) {
  'use strict';

  /**
   * Rerecloud Identity Service is a secure authentication system that reduces the burden of login for users, by enabling them to sign in with a single account into various services provided by Rerecloud. Rerecloud Identity Service is also a gateway to connecting with Rerecloud users and services in a secure manner..<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var AuthIdentityService = require('./index'); // See note below*.
   * var xxxSvc = new AuthIdentityService.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new AuthIdentityService.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['./index'], function(){...}) and put the application logic within the
   * callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new AuthIdentityService.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new AuthIdentityService.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 1.0.1
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The AccessTokenResponse model constructor.
     * @property {module:model/AccessTokenResponse}
     */
    AccessTokenResponse: AccessTokenResponse,
    /**
     * The Account model constructor.
     * @property {module:model/Account}
     */
    Account: Account,
    /**
     * The Application model constructor.
     * @property {module:model/Application}
     */
    Application: Application,
    /**
     * The AuthorizeResponse model constructor.
     * @property {module:model/AuthorizeResponse}
     */
    AuthorizeResponse: AuthorizeResponse,
    /**
     * The CreatedResponse model constructor.
     * @property {module:model/CreatedResponse}
     */
    CreatedResponse: CreatedResponse,
    /**
     * The Error model constructor.
     * @property {module:model/Error}
     */
    Error: Error,
    /**
     * The Holder model constructor.
     * @property {module:model/Holder}
     */
    Holder: Holder,
    /**
     * The HolderApplications model constructor.
     * @property {module:model/HolderApplications}
     */
    HolderApplications: HolderApplications,
    /**
     * The RefreshTokenResponse model constructor.
     * @property {module:model/RefreshTokenResponse}
     */
    RefreshTokenResponse: RefreshTokenResponse,
    /**
     * The RelatedAccountsRef model constructor.
     * @property {module:model/RelatedAccountsRef}
     */
    RelatedAccountsRef: RelatedAccountsRef,
    /**
     * The RelatedApplicationsRef model constructor.
     * @property {module:model/RelatedApplicationsRef}
     */
    RelatedApplicationsRef: RelatedApplicationsRef,
    /**
     * The RelatedGroupsRef model constructor.
     * @property {module:model/RelatedGroupsRef}
     */
    RelatedGroupsRef: RelatedGroupsRef,
    /**
     * The RelatedHolderRef model constructor.
     * @property {module:model/RelatedHolderRef}
     */
    RelatedHolderRef: RelatedHolderRef,
    /**
     * The AccountsApi service constructor.
     * @property {module:api/AccountsApi}
     */
    AccountsApi: AccountsApi,
    /**
     * The AuthenticationApi service constructor.
     * @property {module:api/AuthenticationApi}
     */
    AuthenticationApi: AuthenticationApi,
    /**
     * The HoldersApi service constructor.
     * @property {module:api/HoldersApi}
     */
    HoldersApi: HoldersApi,
    /**
     * The UserEndpointsApi service constructor.
     * @property {module:api/UserEndpointsApi}
     */
    UserEndpointsApi: UserEndpointsApi
  };

  return exports;
}));
