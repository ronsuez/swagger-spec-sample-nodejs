(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', './RelatedAccountsRef', './RelatedApplicationsRef', './RelatedGroupsRef', './RelatedHolderRef'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./RelatedAccountsRef'), require('./RelatedApplicationsRef'), require('./RelatedGroupsRef'), require('./RelatedHolderRef'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.Application = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.RelatedAccountsRef, root.AuthIdentityService.RelatedApplicationsRef, root.AuthIdentityService.RelatedGroupsRef, root.AuthIdentityService.RelatedHolderRef);
  }
}(this, function(ApiClient, RelatedAccountsRef, RelatedApplicationsRef, RelatedGroupsRef, RelatedHolderRef) {
  'use strict';

  /**
   * The Application model module.
   * @module model/Application
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>Application</code>.
   * @alias module:model/Application
   * @class
   * @param id
   * @param name
   * @param description
   * @param status
   * @param createdAt
   * @param modifiedAt
   * @param accounts
   * @param groups
   * @param loginAttemps
   * @param apiKeys
   * @param authTokens
   * @param passwordResetTokens
   * @param holder
   */
  var exports = function(id, name, description, status, createdAt, modifiedAt, accounts, groups, loginAttemps, apiKeys, authTokens, passwordResetTokens, holder) {

    this['id'] = id;
    this['name'] = name;
    this['description'] = description;
    this['status'] = status;
    this['created_at'] = createdAt;
    this['modified_at'] = modifiedAt;
    this['accounts'] = accounts;
    this['groups'] = groups;
    this['loginAttemps'] = loginAttemps;
    this['apiKeys'] = apiKeys;
    this['authTokens'] = authTokens;
    this['passwordResetTokens'] = passwordResetTokens;
    this['holder'] = holder;
  };

  /**
   * Constructs a <code>Application</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Application} obj Optional instance to populate.
   * @return {module:model/Application} The populated <code>Application</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'String');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
      if (data.hasOwnProperty('created_at')) {
        obj['created_at'] = ApiClient.convertToType(data['created_at'], 'Date');
      }
      if (data.hasOwnProperty('modified_at')) {
        obj['modified_at'] = ApiClient.convertToType(data['modified_at'], 'Date');
      }
      if (data.hasOwnProperty('accounts')) {
        obj['accounts'] = ApiClient.convertToType(data['accounts'], [RelatedAccountsRef]);
      }
      if (data.hasOwnProperty('groups')) {
        obj['groups'] = ApiClient.convertToType(data['groups'], [RelatedGroupsRef]);
      }
      if (data.hasOwnProperty('loginAttemps')) {
        obj['loginAttemps'] = ApiClient.convertToType(data['loginAttemps'], [RelatedApplicationsRef]);
      }
      if (data.hasOwnProperty('apiKeys')) {
        obj['apiKeys'] = ApiClient.convertToType(data['apiKeys'], [RelatedApplicationsRef]);
      }
      if (data.hasOwnProperty('authTokens')) {
        obj['authTokens'] = ApiClient.convertToType(data['authTokens'], [RelatedApplicationsRef]);
      }
      if (data.hasOwnProperty('passwordResetTokens')) {
        obj['passwordResetTokens'] = ApiClient.convertToType(data['passwordResetTokens'], [RelatedApplicationsRef]);
      }
      if (data.hasOwnProperty('holder')) {
        obj['holder'] = ApiClient.convertToType(data['holder'], [RelatedHolderRef]);
      }
    }
    return obj;
  }


  /**
   * @member {String} id
   * @default '5fabYFAmuGfqWG6xoILvTf'
   */
  exports.prototype['id'] = '5fabYFAmuGfqWG6xoILvTf';

  /**
   * @member {String} name
   * @default 'My Application'
   */
  exports.prototype['name'] = 'My Application';

  /**
   * @member {String} description
   * @default 'Application description'
   */
  exports.prototype['description'] = 'Application description';

  /**
   * @member {String} status
   * @default 'ENABLED'
   */
  exports.prototype['status'] = 'ENABLED';

  /**
   * @member {Date} created_at
   */
  exports.prototype['created_at'] = undefined;

  /**
   * @member {Date} modified_at
   */
  exports.prototype['modified_at'] = undefined;

  /**
   * @member {Array.<module:model/RelatedAccountsRef>} accounts
   */
  exports.prototype['accounts'] = undefined;

  /**
   * @member {Array.<module:model/RelatedGroupsRef>} groups
   */
  exports.prototype['groups'] = undefined;

  /**
   * @member {Array.<module:model/RelatedApplicationsRef>} loginAttemps
   */
  exports.prototype['loginAttemps'] = undefined;

  /**
   * @member {Array.<module:model/RelatedApplicationsRef>} apiKeys
   */
  exports.prototype['apiKeys'] = undefined;

  /**
   * @member {Array.<module:model/RelatedApplicationsRef>} authTokens
   */
  exports.prototype['authTokens'] = undefined;

  /**
   * @member {Array.<module:model/RelatedApplicationsRef>} passwordResetTokens
   */
  exports.prototype['passwordResetTokens'] = undefined;

  /**
   * @member {Array.<module:model/RelatedHolderRef>} holder
   */
  exports.prototype['holder'] = undefined;




  return exports;
}));
