(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', './RelatedAccountsRef', './RelatedApplicationsRef', './RelatedGroupsRef'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./RelatedAccountsRef'), require('./RelatedApplicationsRef'), require('./RelatedGroupsRef'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.Holder = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.RelatedAccountsRef, root.AuthIdentityService.RelatedApplicationsRef, root.AuthIdentityService.RelatedGroupsRef);
  }
}(this, function(ApiClient, RelatedAccountsRef, RelatedApplicationsRef, RelatedGroupsRef) {
  'use strict';

  /**
   * The Holder model module.
   * @module model/Holder
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>Holder</code>.
   * @alias module:model/Holder
   * @class
   * @param id
   * @param name
   * @param key
   * @param createdAt
   * @param modifiedAt
   * @param applications
   * @param accounts
   * @param groups
   */
  var exports = function(id, name, key, createdAt, modifiedAt, applications, accounts, groups) {

    this['id'] = id;
    this['name'] = name;
    this['key'] = key;
    this['created_at'] = createdAt;
    this['modified_at'] = modifiedAt;
    this['applications'] = applications;
    this['accounts'] = accounts;
    this['groups'] = groups;
  };

  /**
   * Constructs a <code>Holder</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Holder} obj Optional instance to populate.
   * @return {module:model/Holder} The populated <code>Holder</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'String');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('key')) {
        obj['key'] = ApiClient.convertToType(data['key'], 'String');
      }
      if (data.hasOwnProperty('created_at')) {
        obj['created_at'] = ApiClient.convertToType(data['created_at'], 'Date');
      }
      if (data.hasOwnProperty('modified_at')) {
        obj['modified_at'] = ApiClient.convertToType(data['modified_at'], 'Date');
      }
      if (data.hasOwnProperty('applications')) {
        obj['applications'] = ApiClient.convertToType(data['applications'], [RelatedApplicationsRef]);
      }
      if (data.hasOwnProperty('accounts')) {
        obj['accounts'] = ApiClient.convertToType(data['accounts'], [RelatedAccountsRef]);
      }
      if (data.hasOwnProperty('groups')) {
        obj['groups'] = ApiClient.convertToType(data['groups'], [RelatedGroupsRef]);
      }
    }
    return obj;
  }


  /**
   * @member {String} id
   * @default '5f2mgTvyVWBL4dfUATWLk3'
   */
  exports.prototype['id'] = '5f2mgTvyVWBL4dfUATWLk3';

  /**
   * @member {String} name
   * @default 'recloud-tenant'
   */
  exports.prototype['name'] = 'recloud-tenant';

  /**
   * @member {String} key
   * @default 'recloud-tenant'
   */
  exports.prototype['key'] = 'recloud-tenant';

  /**
   * @member {Date} created_at
   */
  exports.prototype['created_at'] = undefined;

  /**
   * @member {Date} modified_at
   */
  exports.prototype['modified_at'] = undefined;

  /**
   * @member {Array.<module:model/RelatedApplicationsRef>} applications
   */
  exports.prototype['applications'] = undefined;

  /**
   * @member {Array.<module:model/RelatedAccountsRef>} accounts
   */
  exports.prototype['accounts'] = undefined;

  /**
   * @member {Array.<module:model/RelatedGroupsRef>} groups
   */
  exports.prototype['groups'] = undefined;




  return exports;
}));
