(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.AuthorizeResponse = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The AuthorizeResponse model module.
   * @module model/AuthorizeResponse
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>AuthorizeResponse</code>.
   * @alias module:model/AuthorizeResponse
   * @class
   */
  var exports = function() {



  };

  /**
   * Constructs a <code>AuthorizeResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/AuthorizeResponse} obj Optional instance to populate.
   * @return {module:model/AuthorizeResponse} The populated <code>AuthorizeResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('code')) {
        obj['code'] = ApiClient.convertToType(data['code'], 'String');
      }
      if (data.hasOwnProperty('state')) {
        obj['state'] = ApiClient.convertToType(data['state'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} code
   */
  exports.prototype['code'] = undefined;

  /**
   * @member {String} state
   */
  exports.prototype['state'] = undefined;




  return exports;
}));
