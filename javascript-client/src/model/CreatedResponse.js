(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.CreatedResponse = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The CreatedResponse model module.
   * @module model/CreatedResponse
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>CreatedResponse</code>.
   * @alias module:model/CreatedResponse
   * @class
   */
  var exports = function() {




  };

  /**
   * Constructs a <code>CreatedResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/CreatedResponse} obj Optional instance to populate.
   * @return {module:model/CreatedResponse} The populated <code>CreatedResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'Integer');
      }
      if (data.hasOwnProperty('headers')) {
        obj['headers'] = ApiClient.convertToType(data['headers'], ['String']);
      }
      if (data.hasOwnProperty('body')) {
        obj['body'] = ApiClient.convertToType(data['body'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {Integer} status
   */
  exports.prototype['status'] = undefined;

  /**
   * @member {Array.<String>} headers
   */
  exports.prototype['headers'] = undefined;

  /**
   * @member {String} body
   */
  exports.prototype['body'] = undefined;




  return exports;
}));
