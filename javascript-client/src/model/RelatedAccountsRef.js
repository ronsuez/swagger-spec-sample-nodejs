(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.RelatedAccountsRef = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The RelatedAccountsRef model module.
   * @module model/RelatedAccountsRef
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>RelatedAccountsRef</code>.
   * @alias module:model/RelatedAccountsRef
   * @class
   * @param href
   */
  var exports = function(href) {

    this['href'] = href;
  };

  /**
   * Constructs a <code>RelatedAccountsRef</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RelatedAccountsRef} obj Optional instance to populate.
   * @return {module:model/RelatedAccountsRef} The populated <code>RelatedAccountsRef</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('href')) {
        obj['href'] = ApiClient.convertToType(data['href'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} href
   * @default 'https://api.authcloud.com/api/v1/accounts/5f2mgTvyVWBL4dfUATWLk3'
   */
  exports.prototype['href'] = 'https://api.authcloud.com/api/v1/accounts/5f2mgTvyVWBL4dfUATWLk3';




  return exports;
}));
