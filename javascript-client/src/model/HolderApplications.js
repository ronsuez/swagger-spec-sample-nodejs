(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', './Application'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Application'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.HolderApplications = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.Application);
  }
}(this, function(ApiClient, Application) {
  'use strict';

  /**
   * The HolderApplications model module.
   * @module model/HolderApplications
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>HolderApplications</code>.
   * @alias module:model/HolderApplications
   * @class
   * @param offset
   * @param limit
   * @param size
   * @param items
   */
  var exports = function(offset, limit, size, items) {

    this['offset'] = offset;
    this['limit'] = limit;
    this['size'] = size;
    this['items'] = items;
  };

  /**
   * Constructs a <code>HolderApplications</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/HolderApplications} obj Optional instance to populate.
   * @return {module:model/HolderApplications} The populated <code>HolderApplications</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('offset')) {
        obj['offset'] = ApiClient.convertToType(data['offset'], 'Integer');
      }
      if (data.hasOwnProperty('limit')) {
        obj['limit'] = ApiClient.convertToType(data['limit'], 'Integer');
      }
      if (data.hasOwnProperty('size')) {
        obj['size'] = ApiClient.convertToType(data['size'], 'Integer');
      }
      if (data.hasOwnProperty('items')) {
        obj['items'] = ApiClient.convertToType(data['items'], [Application]);
      }
    }
    return obj;
  }


  /**
   * @member {Integer} offset
   * @default 0
   */
  exports.prototype['offset'] = 0;

  /**
   * @member {Integer} limit
   * @default 0
   */
  exports.prototype['limit'] = 0;

  /**
   * @member {Integer} size
   * @default 3
   */
  exports.prototype['size'] = 3;

  /**
   * @member {Array.<module:model/Application>} items
   */
  exports.prototype['items'] = undefined;




  return exports;
}));
