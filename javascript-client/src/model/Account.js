(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.Account = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The Account model module.
   * @module model/Account
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>Account</code>.
   * @alias module:model/Account
   * @class
   * @param givenName
   * @param surname
   * @param email
   * @param password
   */
  var exports = function(givenName, surname, email, password) {

    this['givenName'] = givenName;
    this['surname'] = surname;

    this['email'] = email;
    this['password'] = password;

  };

  /**
   * Constructs a <code>Account</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Account} obj Optional instance to populate.
   * @return {module:model/Account} The populated <code>Account</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('givenName')) {
        obj['givenName'] = ApiClient.convertToType(data['givenName'], 'String');
      }
      if (data.hasOwnProperty('surname')) {
        obj['surname'] = ApiClient.convertToType(data['surname'], 'String');
      }
      if (data.hasOwnProperty('username')) {
        obj['username'] = ApiClient.convertToType(data['username'], 'String');
      }
      if (data.hasOwnProperty('email')) {
        obj['email'] = ApiClient.convertToType(data['email'], 'String');
      }
      if (data.hasOwnProperty('password')) {
        obj['password'] = ApiClient.convertToType(data['password'], 'String');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} givenName
   */
  exports.prototype['givenName'] = undefined;

  /**
   * @member {String} surname
   */
  exports.prototype['surname'] = undefined;

  /**
   * @member {String} username
   */
  exports.prototype['username'] = undefined;

  /**
   * @member {String} email
   */
  exports.prototype['email'] = undefined;

  /**
   * @member {String} password
   */
  exports.prototype['password'] = undefined;

  /**
   * @member {String} status
   */
  exports.prototype['status'] = undefined;




  return exports;
}));
