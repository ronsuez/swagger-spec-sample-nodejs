(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.RefreshTokenResponse = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The RefreshTokenResponse model module.
   * @module model/RefreshTokenResponse
   * @version 1.0.1
   */

  /**
   * Constructs a new <code>RefreshTokenResponse</code>.
   * @alias module:model/RefreshTokenResponse
   * @class
   */
  var exports = function() {


  };

  /**
   * Constructs a <code>RefreshTokenResponse</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RefreshTokenResponse} obj Optional instance to populate.
   * @return {module:model/RefreshTokenResponse} The populated <code>RefreshTokenResponse</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('refresh_token')) {
        obj['refresh_token'] = ApiClient.convertToType(data['refresh_token'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} refresh_token
   */
  exports.prototype['refresh_token'] = undefined;




  return exports;
}));
