(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/HolderApplications', '../model/Application', '../model/Holder'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/HolderApplications'), require('../model/Application'), require('../model/Holder'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.HoldersApi = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.HolderApplications, root.AuthIdentityService.Application, root.AuthIdentityService.Holder);
  }
}(this, function(ApiClient, HolderApplications, Application, Holder) {
  'use strict';

  /**
   * Holders service.
   * @module api/HoldersApi
   * @version 1.0.1
   */

  /**
   * Constructs a new HoldersApi. 
   * @alias module:api/HoldersApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the holdersHolderIdApplicationsGet operation.
     * @callback module:api/HoldersApi~holdersHolderIdApplicationsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/HolderApplications} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Retrieve the list of holder Applications
     * An Application is a real-world software application that communicates with Stormpath to offload user management, authentication, and security workflows. Each application that communicates with Stormpath is represented within Stormpath so you may manage its security needs
     * @param {String} holderId 
     * @param {module:api/HoldersApi~holdersHolderIdApplicationsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/HolderApplications}
     */
    this.holdersHolderIdApplicationsGet = function(holderId, callback) {
      var postBody = null;

      // verify the required parameter 'holderId' is set
      if (holderId == undefined || holderId == null) {
        throw "Missing the required parameter 'holderId' when calling holdersHolderIdApplicationsGet";
      }


      var pathParams = {
        'holderId': holderId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = HolderApplications;

      return this.apiClient.callApi(
        '/holders/{holderId}/applications', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the holdersHolderIdApplicationsPost operation.
     * @callback module:api/HoldersApi~holdersHolderIdApplicationsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Application} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a new Application
     * Creates a new Application
     * @param {String} holderId 
     * @param {Object} opts Optional parameters
     * @param {String} opts.appName Desired App name
     * @param {String} opts.appDescription Desired app description
     * @param {module:api/HoldersApi~holdersHolderIdApplicationsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/Application}
     */
    this.holdersHolderIdApplicationsPost = function(holderId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'holderId' is set
      if (holderId == undefined || holderId == null) {
        throw "Missing the required parameter 'holderId' when calling holdersHolderIdApplicationsPost";
      }


      var pathParams = {
        'holderId': holderId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'appName': opts['appName'],
        'appDescription': opts['appDescription']
      };

      var authNames = ['api_key', 'api_key_secret'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = Application;

      return this.apiClient.callApi(
        '/holders/{holderId}/applications', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the holdersHolderIdGet operation.
     * @callback module:api/HoldersApi~holdersHolderIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Holder} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get the Holder info and hyperlinks
     * 
     * @param {String} holderId 
     * @param {module:api/HoldersApi~holdersHolderIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/Holder}
     */
    this.holdersHolderIdGet = function(holderId, callback) {
      var postBody = null;

      // verify the required parameter 'holderId' is set
      if (holderId == undefined || holderId == null) {
        throw "Missing the required parameter 'holderId' when calling holdersHolderIdGet";
      }


      var pathParams = {
        'holderId': holderId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['api_key', 'api_key_secret', 'accessToken'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = Holder;

      return this.apiClient.callApi(
        '/holders/{holderId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
