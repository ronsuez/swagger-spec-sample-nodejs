(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.UserEndpointsApi = factory(root.AuthIdentityService.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * UserEndpoints service.
   * @module api/UserEndpointsApi
   * @version 1.0.1
   */

  /**
   * Constructs a new UserEndpointsApi. 
   * @alias module:api/UserEndpointsApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the authLoginPost operation.
     * @callback module:api/UserEndpointsApi~authLoginPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Login endpoint for users
     * 
     * @param {String} username 
     * @param {String} password 
     * @param {module:api/UserEndpointsApi~authLoginPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.authLoginPost = function(username, password, callback) {
      var postBody = null;

      // verify the required parameter 'username' is set
      if (username == undefined || username == null) {
        throw "Missing the required parameter 'username' when calling authLoginPost";
      }

      // verify the required parameter 'password' is set
      if (password == undefined || password == null) {
        throw "Missing the required parameter 'password' when calling authLoginPost";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'username': username,
        'password': password
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = null;

      return this.apiClient.callApi(
        '/auth/login', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the authRegisterPost operation.
     * @callback module:api/UserEndpointsApi~authRegisterPostCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Register endpoint for users
     * 
     * @param {String} username 
     * @param {String} userEmail 
     * @param {String} password 
     * @param {String} confirmPassword 
     * @param {module:api/UserEndpointsApi~authRegisterPostCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.authRegisterPost = function(username, userEmail, password, confirmPassword, callback) {
      var postBody = null;

      // verify the required parameter 'username' is set
      if (username == undefined || username == null) {
        throw "Missing the required parameter 'username' when calling authRegisterPost";
      }

      // verify the required parameter 'userEmail' is set
      if (userEmail == undefined || userEmail == null) {
        throw "Missing the required parameter 'userEmail' when calling authRegisterPost";
      }

      // verify the required parameter 'password' is set
      if (password == undefined || password == null) {
        throw "Missing the required parameter 'password' when calling authRegisterPost";
      }

      // verify the required parameter 'confirmPassword' is set
      if (confirmPassword == undefined || confirmPassword == null) {
        throw "Missing the required parameter 'confirmPassword' when calling authRegisterPost";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'username': username,
        'userEmail': userEmail,
        'password': password,
        'confirmPassword': confirmPassword
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = null;

      return this.apiClient.callApi(
        '/auth/register', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
