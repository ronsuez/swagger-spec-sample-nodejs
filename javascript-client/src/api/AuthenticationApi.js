(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/AccessTokenResponse', '../model/AuthorizeResponse', '../model/Error', '../model/RefreshTokenResponse'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/AccessTokenResponse'), require('../model/AuthorizeResponse'), require('../model/Error'), require('../model/RefreshTokenResponse'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.AuthenticationApi = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.AccessTokenResponse, root.AuthIdentityService.AuthorizeResponse, root.AuthIdentityService.Error, root.AuthIdentityService.RefreshTokenResponse);
  }
}(this, function(ApiClient, AccessTokenResponse, AuthorizeResponse, Error, RefreshTokenResponse) {
  'use strict';

  /**
   * Authentication service.
   * @module api/AuthenticationApi
   * @version 1.0.1
   */

  /**
   * Constructs a new AuthenticationApi. 
   * @alias module:api/AuthenticationApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the oauthAccessTokenPost operation.
     * @callback module:api/AuthenticationApi~oauthAccessTokenPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AccessTokenResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get Acess token to use Rerecloud API
     * Before your application can access private data using a Rerecloud API, it must obtain an access token that grants access to that API. A single access token can grant varying degrees of access to multiple APIs. A variable parameter called scope controls the set of resources and operations that an access token permits. During the access-token request, your application sends one or more values in the scope parameter.
     * @param {Object} opts Optional parameters
     * @param {String} opts.clientId 
     * @param {String} opts.clientSecret 
     * @param {String} opts.code Code recieved in the authorization step
     * @param {String} opts.accept expected Response format
     * @param {String} opts.grantType auth grant_type [client_credentials, oauth]
     * @param {module:api/AuthenticationApi~oauthAccessTokenPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/AccessTokenResponse}
     */
    this.oauthAccessTokenPost = function(opts, callback) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'clientId': opts['clientId'],
        'clientSecret': opts['clientSecret'],
        'code': opts['code'],
        'accept': opts['accept'],
        'grant_type': opts['grantType']
      };

      var authNames = [];
      var contentTypes = ['application/x-www-form-urlencoded'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = AccessTokenResponse;

      return this.apiClient.callApi(
        '/oauth/access_token', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the oauthAuthorizeGet operation.
     * @callback module:api/AuthenticationApi~oauthAuthorizeGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AuthorizeResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Request user Authorization
     * 
     * @param {String} clientId 
     * @param {String} redirectUri 
     * @param {Object} opts Optional parameters
     * @param {String} opts.scope 
     * @param {String} opts.state 
     * @param {module:api/AuthenticationApi~oauthAuthorizeGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/AuthorizeResponse}
     */
    this.oauthAuthorizeGet = function(clientId, redirectUri, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'clientId' is set
      if (clientId == undefined || clientId == null) {
        throw "Missing the required parameter 'clientId' when calling oauthAuthorizeGet";
      }

      // verify the required parameter 'redirectUri' is set
      if (redirectUri == undefined || redirectUri == null) {
        throw "Missing the required parameter 'redirectUri' when calling oauthAuthorizeGet";
      }


      var pathParams = {
      };
      var queryParams = {
        'clientId': clientId,
        'redirectUri': redirectUri,
        'scope': opts['scope'],
        'state': opts['state']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = AuthorizeResponse;

      return this.apiClient.callApi(
        '/oauth/authorize', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the oauthRefreshAccessTokenPost operation.
     * @callback module:api/AuthenticationApi~oauthRefreshAccessTokenPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/RefreshTokenResponse} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Refresh an access token
     * Access tokens have limited lifetimes. If your application needs access to a Rereca API beyond the lifetime of a single access token, it can obtain a refresh token. A refresh token allows your application to obtain new access tokens.
     * @param {Object} opts Optional parameters
     * @param {String} opts.grantType 
     * @param {String} opts.refreshToken 
     * @param {module:api/AuthenticationApi~oauthRefreshAccessTokenPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/RefreshTokenResponse}
     */
    this.oauthRefreshAccessTokenPost = function(opts, callback) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'grant_type': opts['grantType'],
        'refresh_token': opts['refreshToken']
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json', 'application/xml'];
      var returnType = RefreshTokenResponse;

      return this.apiClient.callApi(
        '/oauth/refreshAccessToken', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
