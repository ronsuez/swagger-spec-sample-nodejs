(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/Account', '../model/Error'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/Account'), require('../model/Error'));
  } else {
    // Browser globals (root is window)
    if (!root.AuthIdentityService) {
      root.AuthIdentityService = {};
    }
    root.AuthIdentityService.AccountsApi = factory(root.AuthIdentityService.ApiClient, root.AuthIdentityService.Account, root.AuthIdentityService.Error);
  }
}(this, function(ApiClient, Account, Error) {
  'use strict';

  /**
   * Accounts service.
   * @module api/AccountsApi
   * @version 1.0.1
   */

  /**
   * Constructs a new AccountsApi. 
   * @alias module:api/AccountsApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the applicationsApplicationIdAccountsLoginAttempsPost operation.
     * @callback module:api/AccountsApi~applicationsApplicationIdAccountsLoginAttempsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Account} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Attemps an account login
     * @param {String} applicationId 
     * @param {module:api/AccountsApi~applicationsApplicationIdAccountsLoginAttempsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/Account}
     */
    this.applicationsApplicationIdAccountsLoginAttempsPost = function(applicationId, callback) {
      var postBody = null;

      // verify the required parameter 'applicationId' is set
      if (applicationId == undefined || applicationId == null) {
        throw "Missing the required parameter 'applicationId' when calling applicationsApplicationIdAccountsLoginAttempsPost";
      }


      var pathParams = {
        'applicationId': applicationId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['api_key', 'api_key_secret'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Account;

      return this.apiClient.callApi(
        '/applications/{applicationId}/accounts/loginAttemps', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the applicationsApplicationIdAccountsPost operation.
     * @callback module:api/AccountsApi~applicationsApplicationIdAccountsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Account} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create a User Account into an application
     * @param {String} applicationId 
     * @param {module:api/AccountsApi~applicationsApplicationIdAccountsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/Account}
     */
    this.applicationsApplicationIdAccountsPost = function(applicationId, callback) {
      var postBody = null;

      // verify the required parameter 'applicationId' is set
      if (applicationId == undefined || applicationId == null) {
        throw "Missing the required parameter 'applicationId' when calling applicationsApplicationIdAccountsPost";
      }


      var pathParams = {
        'applicationId': applicationId
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['api_key', 'api_key_secret'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'application/xml'];
      var returnType = Account;

      return this.apiClient.callApi(
        '/applications/{applicationId}/accounts', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
